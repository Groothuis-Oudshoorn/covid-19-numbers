In this small repository you can find R-code to download and tidy the daily RIVM data on the positive COVID-19 tests.
Moreover the code to scrape the population sizes of the Dutch municipalities. 

Karin Groothuis-Oudshoorn
