# This code is for downloading and extracting the number of inhabitants in all Dutch municipalities.
# Karin Groothuis-Oudshoorn
############################################
# 
# First load the libraries needed.
# This code is run in Rstudio 3.6.3.
library(dplyr)
library(rvest)
library(jsonlite)
library(data.table)
library(lubridate)
library(stringr)
library(tidyverse)
library(stringr)
library(stringi)
#

# 
adress <- "https://www.regioatlas.nl/gemeenten"
urls_html1 <- read_html(adress)
temp <- html_text(urls_html1)
temp2 <- sub('.*Inwoneraantal', '', temp)
temp2 <- sub('.*oplopend', '', temp2)
temp2 <- sub('Over RegioAtlas.*', '',temp2)
temp2 <- sub('.*Aa en Hunze','Aa en Hunze',temp2)

temp3 <- gsub("\r*","",temp2)
temp3 <- gsub("\t","",temp3)
temp3 <- gsub("›","",temp3)
temp3 <- gsub("\\.","",temp3)

vector_str_fixed <- as.vector(str_split_fixed(temp3, pattern = "\n", n = 2000))
vector_str1 <- stri_remove_empty(vector_str_fixed)
vector_str2 <- str_replace_all(vector_str1, "\\s+", " ")
vector_str3 <- vector_str2[str_length(vector_str2)>1]

n <- length(vector_str3)/3
gemeenten <- tibble(naam = as.character(vector_str3[1+3*(0:(n-1))]),
                          provincie = as.character(vector_str3[2 + 3*(0:(n-1))]),
                          aantal = as.numeric(vector_str3[3*(1:n)]))
  
gemeenten <- gemeenten %>% mutate(naam = ifelse(naam == "Hengelo (O)","Hengelo",naam),
                                  naam = ifelse(naam == "Nordeast-Fryslân","Noardeast-Fryslân",naam),
                                  naam = ifelse(naam == "Súdwest Fryslân","Súdwest-Fryslân",naam))

# the result is a dataframe with the columns: naam, provincie en aantal. 